package by.inventain.interoct.boardroom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author aleks on 3/21/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class BoardroomControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFormBookingCalendar() throws Exception {
        this.mockMvc.perform(post("/book").contentType(MediaType.APPLICATION_JSON).content(
                "{\n" +
                        "  \"cmpnyStartH\": \"0900\",\n" +
                        "  \"cmpnyEndH\": \"1730\",\n" +
                        "  \"bookings\": [\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-17 10:17:06\",\n" +
                        "      \"startDateTime\": \"2011-03-21 09:00\",\n" +
                        "      \"employeeId\": \"EMP001\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-16 12:34:56\",\n" +
                        "      \"startDateTime\": \"2011-03-21 09:00\",\n" +
                        "      \"employeeId\": \"EMP002\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-16 09:28:23\",\n" +
                        "      \"startDateTime\": \"2011-03-22 14:00\",\n" +
                        "      \"employeeId\": \"EMP003\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-17 11:23:45\",\n" +
                        "      \"startDateTime\": \"2011-03-22 16:00\",\n" +
                        "      \"employeeId\": \"EMP004\",\n" +
                        "      \"durationH\": \"1\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-15 17:29:12\",\n" +
                        "      \"startDateTime\": \"2011-03-21 16:00\",\n" +
                        "      \"employeeId\": \"EMP005\",\n" +
                        "      \"durationH\": \"3\"\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}\n"
        )).andDo(print()).andExpect(status().isOk()).andExpect(content().json(
                "{\n" +
                        "  \"2011-03-21\": [\n" +
                        "    {\n" +
                        "      \"startH\": \"09:00\",\n" +
                        "      \"endH\": \"11:00\",\n" +
                        "      \"employeeId\": \"EMP002\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"2011-03-22\": [\n" +
                        "    {\n" +
                        "      \"startH\": \"14:00\",\n" +
                        "      \"endH\": \"16:00\",\n" +
                        "      \"employeeId\": \"EMP003\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"startH\": \"16:00\",\n" +
                        "      \"endH\": \"17:00\",\n" +
                        "      \"employeeId\": \"EMP004\"\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}")
        );
    }

    @Test
    public void shouldReturnEmptyResponseIncorrectWorkingHours() throws Exception {
        this.mockMvc.perform(post("/book").contentType(MediaType.APPLICATION_JSON).content(
                "{\n" +
                        "  \"cmpnyStartH\": \"1000\",\n" +
                        "  \"cmpnyEndH\": \"0900\",\n" +
                        "  \"bookings\": [\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-17 10:17:06\",\n" +
                        "      \"startDateTime\": \"2011-03-21 09:00\",\n" +
                        "      \"employeeId\": \"EMP001\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-16 12:34:56\",\n" +
                        "      \"startDateTime\": \"2011-03-21 09:00\",\n" +
                        "      \"employeeId\": \"EMP002\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-16 09:28:23\",\n" +
                        "      \"startDateTime\": \"2011-03-22 14:00\",\n" +
                        "      \"employeeId\": \"EMP003\",\n" +
                        "      \"durationH\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-17 11:23:45\",\n" +
                        "      \"startDateTime\": \"2011-03-22 16:00\",\n" +
                        "      \"employeeId\": \"EMP004\",\n" +
                        "      \"durationH\": \"1\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"submissionDateTime\": \"2011-03-15 17:29:12\",\n" +
                        "      \"startDateTime\": \"2011-03-21 16:00\",\n" +
                        "      \"employeeId\": \"EMP005\",\n" +
                        "      \"durationH\": \"3\"\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}\n"
        )).andDo(print()).andExpect(status().isOk()).andExpect(content().json(
                "{}")
        );
    }

    @Test
    public void shouldReturnEmptyResponseEmptyBookingList() throws Exception {
        this.mockMvc.perform(post("/book").contentType(MediaType.APPLICATION_JSON).content(
                "{\n" +
                        "  \"cmpnyStartH\": \"0900\",\n" +
                        "  \"cmpnyEndH\": \"1700\",\n" +
                        "  \"bookings\": []\n" +
                        "}\n"
        )).andDo(print()).andExpect(status().isOk()).andExpect(content().json(
                "{}")
        );
    }
}