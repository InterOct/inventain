package by.inventain.interoct.boardroom.service;

import by.inventain.interoct.boardroom.domain.request.BoardRoomRequest;
import by.inventain.interoct.boardroom.domain.request.BookingRequest;
import by.inventain.interoct.boardroom.domain.response.BookingRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BoardroomService
 *
 * @author aleks on 3/21/2017.
 */

@Service
public class BoardroomService {

    public BoardroomService() {
    }

    /**
     * @param request to transform
     * @return list of meeting groped by day
     */
    public Map<LocalDate, List<BookingRecord>> formBookingCalendar(BoardRoomRequest request) {
        final Map<LocalDate, List<BookingRecord>> map = new TreeMap<>();

        for (BookingRequest booking : sortAndFilter(request)) {
            final LocalTime startTime = booking.getStartDateTime().toLocalTime();
            final LocalTime endTime = startTime.plus(Duration.ofHours(booking.getDurationH()));

            final List<BookingRecord> records = map.computeIfAbsent(
                    booking.getStartDateTime().toLocalDate(), k -> new ArrayList<>());
            if (records.parallelStream().noneMatch(
                    record -> isOverlap(startTime, endTime, record.getStartH(), record.getEndH()))) {
                records.add(new BookingRecord(startTime, endTime, booking.getEmployeeId()));
            }
        }
        map.values().forEach(rec -> rec.sort(Comparator.comparing(BookingRecord::getStartH)));
        return map;
    }

    /**
     * sorts by submission time and filters out booking requests that fall outside office hour
     */
    private List<BookingRequest> sortAndFilter(BoardRoomRequest rq) {
        return rq.getBookings().parallelStream()
                .filter(
                        booking -> {
                            final LocalTime startTime = booking.getStartDateTime().toLocalTime();
                            return startTime.compareTo(rq.getCmpnyStartH()) >= 0
                                    && startTime.plus(Duration.ofHours(booking.getDurationH())).compareTo(rq.getCmpnyEndH()) <= 0;
                        }
                )
                .sorted(Comparator.comparing(BookingRequest::getSubmissionDateTime))
                .collect(Collectors.toList());
    }

    private boolean isOverlap(LocalTime start1, LocalTime end1, LocalTime start2, LocalTime end2) {
        long maxStartTime = Math.max(start1.toNanoOfDay(), start2.toNanoOfDay());
        long minEndTime = Math.min(end1.toNanoOfDay(), end2.toNanoOfDay());
        return maxStartTime < minEndTime;
    }
}
