package by.inventain.interoct.boardroom.controller;

import by.inventain.interoct.boardroom.domain.request.BoardRoomRequest;
import by.inventain.interoct.boardroom.domain.response.BookingRecord;
import by.inventain.interoct.boardroom.service.BoardroomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
public class BoardroomController {

    private static final Logger log = LoggerFactory.getLogger(BoardroomController.class);

    @Autowired
    private BoardroomService service;

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public Map<LocalDate, List<BookingRecord>> book(@Valid @RequestBody BoardRoomRequest boardRoomRequest) {
        return service.formBookingCalendar(boardRoomRequest);
    }

}
