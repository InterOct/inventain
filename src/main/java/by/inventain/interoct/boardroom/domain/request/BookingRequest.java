
package by.inventain.interoct.boardroom.domain.request;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * BookingRequest
 *
 * @author aleks
 */
public class BookingRequest {

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime submissionDateTime;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startDateTime;
    @NotNull
    private String employeeId;
    @NotNull
    private Integer durationH;

    public LocalDateTime getSubmissionDateTime() {
        return submissionDateTime;
    }

    public void setSubmissionDateTime(LocalDateTime submissionDateTime) {
        this.submissionDateTime = submissionDateTime;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Integer getDurationH() {
        return durationH;
    }

    public void setDurationH(Integer durationH) {
        this.durationH = durationH;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingRequest bookingRequest = (BookingRequest) o;
        return Objects.equals(submissionDateTime, bookingRequest.submissionDateTime) &&
                Objects.equals(employeeId, bookingRequest.employeeId) &&
                Objects.equals(startDateTime, bookingRequest.startDateTime) &&
                Objects.equals(durationH, bookingRequest.durationH);
    }

    @Override
    public int hashCode() {
        return Objects.hash(submissionDateTime, employeeId, startDateTime, durationH);
    }

    @Override
    public String toString() {
        return "BookingRequest{" +
                "submissionDateTime:" + submissionDateTime + '\'' +
                ", employeeId:" + employeeId + '\'' +
                ", startDateTime:" + startDateTime + '\'' +
                ", durationH:" + durationH +
                '}';
    }
}
