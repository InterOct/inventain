package by.inventain.interoct.boardroom.domain.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalTime;

/**
 * @author aleks
 */
public class BookingRecord {

    @JsonFormat(pattern = "HH:mm")
    private LocalTime startH;
    @JsonFormat(pattern = "HH:mm")
    private LocalTime endH;
    private String employeeId;

    public BookingRecord(LocalTime startH, LocalTime endH, String employeeId) {
        this.startH = startH;
        this.endH = endH;
        this.employeeId = employeeId;
    }

    public LocalTime getStartH() {
        return startH;
    }

    public void setStartH(LocalTime startH) {
        this.startH = startH;
    }

    public LocalTime getEndH() {
        return endH;
    }

    public void setEndH(LocalTime endH) {
        this.endH = endH;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
