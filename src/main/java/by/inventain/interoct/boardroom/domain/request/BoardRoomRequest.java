
package by.inventain.interoct.boardroom.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

public class BoardRoomRequest {

    @NotNull
    @JsonFormat(pattern = "HHmm")
    private LocalTime cmpnyStartH;
    @NotNull
    @JsonFormat(pattern = "HHmm")
    private LocalTime cmpnyEndH;
    @NotNull
    private List<BookingRequest> bookings;

    public LocalTime getCmpnyStartH() {
        return cmpnyStartH;
    }

    public void setCmpnyStartH(LocalTime cmpnyStartH) {
        this.cmpnyStartH = cmpnyStartH;
    }

    public LocalTime getCmpnyEndH() {
        return cmpnyEndH;
    }

    public void setCmpnyEndH(LocalTime cmpnyEndH) {
        this.cmpnyEndH = cmpnyEndH;
    }

    public List<BookingRequest> getBookings() {
        return bookings;
    }

    public void setBookings(List<BookingRequest> bookings) {
        this.bookings = bookings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardRoomRequest that = (BoardRoomRequest) o;
        return Objects.equals(cmpnyStartH, that.cmpnyStartH) &&
                Objects.equals(cmpnyEndH, that.cmpnyEndH) &&
                Objects.equals(bookings, that.bookings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cmpnyStartH, cmpnyEndH, bookings);
    }

    @Override
    public String toString() {
        return "BoardRoomRequest{" +
                "cmpnyStartH='" + cmpnyStartH + '\'' +
                ", cmpnyEndH='" + cmpnyEndH + '\'' +
                ", bookings=" + bookings +
                '}';
    }
}
