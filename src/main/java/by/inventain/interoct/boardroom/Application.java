package by.inventain.interoct.boardroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "by.inventain.interoct")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
